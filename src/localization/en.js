import { is } from 'shabbat-logic';

export default {
	translate: {
		title: 'Is It Shabbat?',
		copyright: '© 2019, James Fulford and Jessica Fulford.',
		status: {
			[is.SHABBAT]: 'Yes!',
			[is.NOT_SHABBAT]: 'No...',
			[is.CANDLELIGHTING]: 'Almost...',
		},
		endEventName: {
			[is.SHABBAT]: 'until Shabbat ends',
			[is.NOT_SHABBAT]: 'until candle lighting',
			[is.CANDLELIGHTING]: 'until Shabbat begins',
		},
		startEventName: {
			[is.SHABBAT]: 'Shabbat ends',
			[is.NOT_SHABBAT]: 'Candle Lighting',
			[is.CANDLELIGHTING]: 'Shabbat begins',
		},
		screens: {
			settings: 'Settings',
			info: 'Credits',
		},
	},
};
