export const colors = {
	background: '#3D2F40',
	textMain: '#F4BDFF',
	textSubtle: '#7A5F7F',
};
export const fonts = {
	primary: 'FredokaOne',
};
export const size = {
	title: 72,
	secondary: 22,
	subtitle: 18,
};
